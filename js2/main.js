var countries = [
    {name: 'Беларусь', capital: 'Минск'},
    {name: 'Россия', capital: 'Москва'},
    {name: 'Украина', capital: 'Киев'},
    {name: 'Мексика', capital: 'Мехико'},
    {name: 'Япония', capital: 'Токио'}
];

function askCountry() {
    return prompt('введите название страны');
}

function askCapital() {
    return prompt('введите название столицы');
}

function addCountry() {
    var country = {};
    country.name = askCountry();
    country.capital = askCapital();
    console.log(country);
    countries.push(country);
    console.log(countries);
}

function showCountries() {
    console.log(countries);
}

function getCountryInfo() {
    var requestCountryName = askCountry();
    console.log(requestCountryName);
    var requestCountry = countries.find(function(country) {
        return requestCountryName === country.name;
    });
    console.log(requestCountry);
    if(requestCountry) {
        alert('столица: ' + requestCountry.capital);
    } else {
        alert('нет информации о стране');
    }
}

function delCountryInfo() {
    var delCountryName = askCountry();
    console.log(delCountryName);
    var delCountry = countries.filter(function(country) {
        return delCountryName !== country.name;
    });
    console.log(delCountry);
    if (countries.length === delCountry.length) {
        alert('страна' +  ' ' + delCountryName + ' ' + 'не найдена');
    } else {
        alert('страна' + ' ' + delCountryName + ' ' + 'удалена');
    }
}

var enterInfoButton = document.querySelector('button:nth-child(1)');
console.log(enterInfoButton);
enterInfoButton.addEventListener('click', addCountry);

var getInfoCountryBtn = document.querySelector('button:nth-child(2)');
console.log(getInfoCountryBtn);
getInfoCountryBtn.addEventListener('click', getCountryInfo);

var showCollectionBtn = document.querySelector('button:nth-child(3)');
console.log(showCollectionBtn);
showCollectionBtn.addEventListener('click', showCountries);

var delInfoBtn = document.querySelector('button:nth-child(4)');
console.log(delInfoBtn);
delInfoBtn.addEventListener('click', delCountryInfo);