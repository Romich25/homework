var links = document.querySelectorAll('.main-link');
var boxes = document.querySelectorAll('.main-text');
 
function readMore(event) {
  link = event.target;
  box = link.parentElement;
  box.classList.toggle('collapsed');
  link.classList.toggle('absolute');
  
  var linkContains = link.classList.contains('absolute');
  
  linkContains === true ? link.innerHTML = 'read more...' : link.innerHTML = 'read less';
}

links.forEach(function(link){
    link.addEventListener('click', readMore);
}); 

